package scripts.wastedbro.api.input

import org.tribot.api.interfaces.Clickable
import scripts.wastedbro.api.input.dax.DaxMouse

/**
 * Provides a utility for clicking or hovering OSRS entities using a chosen mousen type
 */
object WastedMouse
{
    var mouseType = MouseType.DAX

    fun click(clickable: Clickable, vararg actions: String) = when (mouseType)
    {
        MouseType.TRIBOT -> clickable.click(*actions)
        MouseType.DAX -> DaxMouse.click(clickable, *actions)
        MouseType.TOUCH -> throw NotImplementedError("Touch mouse not implemented")
    }

    fun hover(clickable: Clickable, vararg actions: String): Boolean
    {
        return when (mouseType)
        {
            MouseType.TRIBOT -> clickable.hover()
            MouseType.DAX -> DaxMouse.hover(clickable, *actions)
            MouseType.TOUCH -> throw NotImplementedError("Touch mouse not implemented")
        }
    }
}