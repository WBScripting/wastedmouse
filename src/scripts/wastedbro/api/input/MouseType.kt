package scripts.wastedbro.api.input

enum class MouseType {
    DAX,
    TRIBOT,
    TOUCH
}