package scripts.wastedbro.api.input

import org.tribot.api.interfaces.Clickable

fun Clickable.wastedClick(vararg actions: String) = WastedMouse.click(this, *actions)

fun Clickable.wastedHover(vararg actions: String) = WastedMouse.hover(this, *actions)